APP="desktop"

### Docker settings.
IMAGE="desk.example.org"
CONTAINER="desk.example.org"
DOMAIN="desk.example.org"

### Email for getting a letsencrypt SSL certificate.
SSL_CERT_EMAIL=admin@example.org

### Forwarded ports
GUAC_PORT="444"
PORTS="$GUAC_PORT:443"
#X2GO_PORT="2202"
#PORTS="$GUAC_PORT:443 $X2GO_PORT:22"

# Access the server from the web with Guacamole
# https://guacamole.apache.org/doc/gug/using-guacamole.html
# It can be accessed on: https://$DOMAIN/guac/
GUAC_ADMIN="admin"
GUAC_PASS="pass"
GUAC_USER_NAME="student"
GUAC_USER_PASS="student"
GUAC_MAX_CONNECTIONS="50"

## Epoptes admins. Uncomment to enable.
#EPOPTES_USERS="user1 user2"

## Admin account. Uncomment to enable.
#ADMIN_USER="admin"
#ADMIN_PASS="pass"

### SMTP server for sending notifications. You can build an SMTP server
### as described here:
### https://gitlab.com/docker-scripts/postfix/blob/master/INSTALL.md
### Comment out if you don't have a SMTP server and want to use
### a gmail account (as described below).
#SMTP_SERVER="smtp.example.org"
#SMTP_DOMAIN="example.org"

### Gmail account for notifications. This will be used by ssmtp.
### You need to create an application specific password for your account:
### https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882
#GMAIL_ADDRESS=username@gmail.org
#GMAIL_PASSWD=hdfhfdjkfglk

