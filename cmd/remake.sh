cmd_remake_help() {
    cat <<_EOF
    remake
        Reconstruct again the container, preserving the existing data.

_EOF
}

cmd_remake() {
    # backup
    ds users backup

    # reinstall
    ds remove
    ds make
    ds restart
    ds wsproxy ssl-cert

    # restore
    local datestamp=$(date +%Y%m%d)
    ds users restore backup/users-$datestamp.tgz
}
